#include <stdio.h>
#include <math.h>

float F(int a, int b){
	float c = 1.0;
	if(b==0){ c= 1.0/sqrt(2.0);}
		
	return c * cos((2.0*a+1.0)/16.0 *b*M_PI);
}

void iDCT_8x8(float in[], float out[]){
	float D[8][8];
	
	for(int y=0; y<8; y++){
		for(int u=0 ; u<8; u++){
			D[y][u] = 
				  ( in[8*0 +u] * F(y,0) ) 
				+ ( in[8*1 +u] * F(y,1) ) 
				+ ( in[8*2 +u] * F(y,2) ) 
				+ ( in[8*3 +u] * F(y,3) ) 
				+ ( in[8*4 +u] * F(y,4) ) 
				+ ( in[8*5 +u] * F(y,5) ) 
				+ ( in[8*6 +u] * F(y,6) ) 
				+ ( in[8*7 +u] * F(y,7) )	;
		}
	}
	//////////////////////////////////////////
	for(int y=0; y<8; y++){
		for(int x=0; x<8; x++){
			out[8*y+x] = .25 * (
				  ( D[y][0] * F(x,0) )
				+ ( D[y][1] * F(x,1) ) 
				+ ( D[y][2] * F(x,2) ) 
				+ ( D[y][3] * F(x,3) ) 
				+ ( D[y][4] * F(x,4) ) 
				+ ( D[y][5] * F(x,5) ) 
				+ ( D[y][6] * F(x,6) ) 
				+ ( D[y][7] * F(x,7) )	)	;
		}
	}
}

void printMatrix_8x8(float mat[]){
	for(int col = 0; col<8; col++){
		for(int row = 0; row<8; row++){
			printf("%f \t", mat[row+8*col]);
		}
		printf("\n");
	}
}


int main(void){
	float out[64]; 
	float  in[64]={
		1	,2	,3	,4	,5	,6	,7	,8	,
		9	,10	,11	,12	,13	,14	,15	,16	,
		17	,18	,19	,20	,21	,22	,23	,24	,
		25	,26	,27	,28	,29	,30	,31	,32	,
		33	,34	,35	,36	,37	,38	,39	,40	,
		41	,42	,43	,44	,45	,46	,47	,48	,
		49	,50	,51	,52	,53	,54	,55	,56	,
		57	,58	,59	,60	,61	,62	,63	,64	};
 
	printf("Input: \n");
	printMatrix_8x8(in);	

	iDCT_8x8(in, out);
	
	printf("Output:\n");
	printMatrix_8x8(out);

	return 0;
}

